using Toybox.ActivityRecording;
using Toybox.System as Sys;
using Toybox.WatchUi as Ui;

class MenuSaveDialog extends MenuGeneric {
    function initialize() {
    	MenuGeneric.initialize();

    	setTitle(WatchUi.loadResource(Rez.Strings.settings));
    }

    function updateMenu() {
        addMenuItem(WatchUi.loadResource(Rez.Strings.save_dialog_resume), null, :resume);
        addMenuItem(WatchUi.loadResource(Rez.Strings.save_dialog_save), null, :save);
        addMenuItem(WatchUi.loadResource(Rez.Strings.save_dialog_discard), null, :discard);
    }
}



class MenuSaveDialogDelegate extends MenuGenericDelegate {
    function initialize() {
        MenuGenericDelegate.initialize();
    }

    function onMenuItem(item) {
        if (item == :resume) {
        	$.session.start();
        } else if (item == :save) {
        	$.session.save();
			$.session = null;
			$.sessionSport = null;
			$.sessionSubSport = null;
        } else if (item == :discard) {
        	try {
        		$.session.discard();
        	} catch (ex) {
        	}
			$.session = null;
			$.sessionSport = null;
			$.sessionSubSport = null;
        }

		return true;
    }

}