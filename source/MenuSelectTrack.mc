using Toybox.Application as App;
using Toybox.System as Sys;
using Toybox.WatchUi as WatchUi;

class MenuSelectTrack extends MenuGeneric {
    function initialize() {
    	MenuGeneric.initialize();

    	setTitle(Rez.Strings.select_track);
    }

    function updateMenu() {
    	var available_tracks = get_all_available_tracks();

    	if (available_tracks == null ||
    			available_tracks.size() == 0) {
    		addMenuItem(
    				WatchUi.loadResource(Rez.Strings.no_tracks_available),
    				"",
    				null);
    	} else {
    		addMenuItem(
    				WatchUi.loadResource(Rez.Strings.track_direction),
    				$.trackDirectionForward ?
    						WatchUi.loadResource(Rez.Strings.forward) :
    						WatchUi.loadResource(Rez.Strings.backward),
    				:track_direction);
	    	for (var i=0; i<available_tracks.size(); i+=2) {
	    		addMenuItem(
	    				available_tracks[i],
	    				available_tracks[i+1].format("%0.2f") + " km",
	    				available_tracks[i]);
	    	}
	    }
    }
}



class MenuSelectTrackDelegate extends MenuGenericDelegate {
	function initialize() {
		MenuGenericDelegate.initialize();
	}

    function onMenuItem(item) {
    	if (item == :track_direction) {
			WatchUi.pushView(
					new MenuPicker(null, 1001),
					new MenuPickerDelegate(null, 1001),
					WatchUi.SLIDE_IMMEDIATE);

    		return false;
    	} else if (item) {
    		// item non-null
    		var old_track_name = $.currentTrackName;
    		$.currentTrackName = item;
    		$.setProperty("current_track_name", $.currentTrackName);
    		$.trackDirectionForward = true;
    		$.setProperty("track_direction_forward", $.trackDirectionForward);
			decode_property_string();
			App.getApp().data_screen.updateLayout();
    	}

    	return true; // return true to pop view when using Menu2
    }
}
