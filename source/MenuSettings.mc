using Toybox.Application;
using Toybox.System as Sys;
using Toybox.WatchUi as WatchUi;

class MenuSettings extends MenuGeneric {
    function initialize() {
    	MenuGeneric.initialize();
    	
    	setTitle(WatchUi.loadResource(Rez.Strings.settings));
    }
    
    function updateMenu() {
        addMenuItem(
        		WatchUi.loadResource(Rez.Strings.select_track),
        		$.currentTrackName,
        		:item_select_track);
        addMenuItem(
        		WatchUi.loadResource(Rez.Strings.config_data_screens),
        		"",
        		:item_data_screens);
        addMenuItem(
        		WatchUi.loadResource(Rez.Strings.show_limits),
        		$.layoutShowLimits ?
        				WatchUi.loadResource(Rez.Strings.graph_grid_on) : 
        				WatchUi.loadResource(Rez.Strings.graph_grid_off),
        		:item_show_limits);
    }
}



class MenuSettingsDelegate extends MenuGenericDelegate {
	function initialize() {
		MenuGenericDelegate.initialize();
	}
	
    function onMenuItem(item) {
        if (item == :item_select_track) {
            WatchUi.pushView(
            		new MenuSelectTrack(),
            		new MenuSelectTrackDelegate(),
            		WatchUi.SLIDE_IMMEDIATE);
        } else if (item == :item_data_screens) {
        	WatchUi.pushView(
        			new MenuConfigDataScreens(),
        			new MenuConfigDataScreensDelegate(),
        			WatchUi.SLIDE_IMMEDIATE);
        }else if (item == :item_show_limits) {
        	$.layoutShowLimits = !$.layoutShowLimits;
        	Application.getApp().data_screen.requestUpdate();
			// store new settings on system memory
			$.setProperty("show_limits", $.layoutShowLimits);
			return true; // popView for Menu devices
        }
        
        return false; // return true to pop view when using Menu2
    }
}
