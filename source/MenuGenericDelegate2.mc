using Toybox.WatchUi as WatchUi;

class MenuGenericDelegate extends WatchUi.Menu2InputDelegate {
    function initialize() {
        Menu2InputDelegate.initialize();
    }
	
	function onSelect(item) {
		// pop the current view if onMenuItem() returns true
		if(self.onMenuItem(item.getId())) {
			WatchUi.popView(WatchUi.SLIDE_IMMEDIATE);
		}
	}
}
