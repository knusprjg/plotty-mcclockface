using Toybox.WatchUi as WatchUi;

class MenuGeneric extends WatchUi.Menu {
    function initialize() {
    	WatchUi.Menu.initialize();
    	self.updateMenu();
    }
    
    function addMenuItem(name, sublabel, identifier) {
        addItem(
        	name,
        	identifier
        );
    }
}
