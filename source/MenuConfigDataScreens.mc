using Toybox.System as Sys;
using Toybox.WatchUi as WatchUi;

class MenuConfigDataScreens extends MenuGeneric {
    function initialize() {
    	MenuGeneric.initialize();

    	setTitle(WatchUi.loadResource(Rez.Strings.config_data_screens));
    }

    function updateMenu() {
        // Menu point for choosing number of data screens
        addMenuItem(
        		WatchUi.loadResource(Rez.Strings.num_data_screens),
        		$.layoutNumDataScreens.toString(),
        		0);

        // Menu points to config data screens
        for (var i=1; i<=$.layoutNumDataScreens; i+=1) {
	       	addMenuItem(
	       			WatchUi.loadResource(Rez.Strings.data_screen) + " " + i,
	       			"",
        			i);
        }
    }
}



class MenuConfigDataScreensDelegate extends MenuGenericDelegate {
    function initialize() {
        MenuGenericDelegate.initialize();
    }

    function onMenuItem(item) {
    	if (item == 0) {
    		WatchUi.pushView(
    				new MenuPicker(null, 1000),
    				new MenuPickerDelegate(null, 1000),
    				WatchUi.SLIDE_IMMEDIATE);
    	} else {
    		// item stands for the data screen number (1 to 5)
    		WatchUi.pushView(
    				new MenuConfigDataScreen(item-1),
    				new MenuConfigDataScreenDelegate(item-1),
    				WatchUi.SLIDE_IMMEDIATE);
    	}

		return false; // return true to pop view when using Menu2
    }
}
