using Toybox.ActivityRecording as AR;
using Toybox.Application;
using Toybox.System as Sys;
using Toybox.WatchUi as Ui;

class MenuSelectActivity extends MenuGeneric {
    function initialize() {
    	MenuGeneric.initialize();

    	setTitle(WatchUi.loadResource(Rez.Strings.activity));
    }

    function updateMenu() {
        addMenuItem(WatchUi.loadResource(Rez.Strings.mtb), null, 0);
        addMenuItem(WatchUi.loadResource(Rez.Strings.cycling), null, 1);
        addMenuItem(WatchUi.loadResource(Rez.Strings.running), null, 2);
        addMenuItem(WatchUi.loadResource(Rez.Strings.xc_ski), null, 3);
        addMenuItem(WatchUi.loadResource(Rez.Strings.alpine_ski), null, 4);
        addMenuItem(WatchUi.loadResource(Rez.Strings.hiking), null, 5);
        addMenuItem(WatchUi.loadResource(Rez.Strings.generic), null, 5);
    }
}



class MenuSelectActivityDelegate extends MenuGenericDelegate {
    function initialize() {
        MenuGenericDelegate.initialize();
    }

    function onMenuItem(item) {
	    $.sessionSport = [
	    		AR.SPORT_CYCLING,
	    		AR.SPORT_CYCLING,
	    		AR.SPORT_RUNNING,
	    		AR.SPORT_CROSS_COUNTRY_SKIING,
	    		AR.SPORT_ALPINE_SKIING,
	    		AR.SPORT_HIKING,
	    		AR.SPORT_GENERIC][item];
	    $.sessionSubSport =
	    		(item == 0) ?
	    		AR.SUB_SPORT_MOUNTAIN :
	    		AR.SUB_SPORT_GENERIC;

		Application.getApp().data_screen.drawCircle = 1;
		Application.getApp().data_screen.uiInitWatchFace = 1;

		return true;
    }

}