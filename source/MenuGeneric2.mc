using Toybox.WatchUi as WatchUi;
using Toybox.System as Sys;

class MenuGeneric extends WatchUi.Menu2 {
    function initialize() {
    	WatchUi.Menu2.initialize({});
    }
    
    function resetMenu () {
    	while (self.getItem(0) != null) {
    		self.deleteItem(0);
    	}
    }
    
    function onShow() {
    	self.resetMenu();
    	self.updateMenu();
    }
    
    function addMenuItem(name, sublabel, identifier) {
        addItem(
        	new WatchUi.MenuItem(
        		name,
        		sublabel,
        		identifier,
        		null
        	)
        );
    }
}
