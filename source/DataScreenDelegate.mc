using Toybox.ActivityRecording;
using Toybox.Application as App;
using Toybox.Attention as Attention;
using Toybox.WatchUi as Ui;
using Toybox.System as Sys;
using Toybox.PersistedContent as Content;

class DataScreenDelegate extends Ui.BehaviorDelegate {
	function initialize () {
		BehaviorDelegate.initialize();
	}

	function onSelect() {
    	if (Toybox has :ActivityRecording) {
			if ((($.session == null) || ($.session.isRecording() == false)) && ($.sessionSport == null)) {
    			App.getApp().data_screen.enableGps();
        		Ui.pushView(
        				new MenuSelectActivity(),
        				new MenuSelectActivityDelegate(),
        				Ui.SLIDE_IMMEDIATE);
        	} else if (($.session == null) || ($.session.isRecording() == false)) {
				$.session = ActivityRecording.createSession(
					{
						:name=>"Elevation",
						:sport=>$.sessionSport,
						:subSport=>$.sessionSubSport
					});
				$.session.start();
				App.getApp().data_screen.drawCircle = 0;
				App.getApp().data_screen.uiInitWatchFace = 1;
			} else if (($.session != null) && $.session.isRecording()) {
				$.session.stop();
				if (Attention has :vibrate) {
					Attention.vibrate([new Attention.VibeProfile(50, 300)]);
				}
				Ui.pushView(
						new MenuSaveDialog(),
						new MenuSaveDialogDelegate(),
						Ui.SLIDE_IMMEDIATE);
			}
		}

		// updateLayout to redraw graph
    	App.getApp().data_screen.updateLayout();
		return true;
	}

    function onMenu() {
    	WatchUi.pushView(
	    	new MenuSettings(),
	    	new MenuSettingsDelegate(),
	    	WatchUi.SLIDE_IMMEDIATE);
        return true;
    }

    function onPreviousPage() {
    	$.layoutCurrentDataScreen = ($.layoutCurrentDataScreen + $.layoutNumDataScreens - 1) % $.layoutNumDataScreens;
    	//log("Changing Datascreen: " + ($.layoutCurrentDataScreen+1) + " / " + $.layoutNumDataScreens);
    	App.getApp().data_screen.updateLayout();
    	App.getApp().data_screen.requestUpdate();
    }

    function onNextPage() {
    	$.layoutCurrentDataScreen = ($.layoutCurrentDataScreen + 1) % $.layoutNumDataScreens;
    	//log("Changing Datascreen: " + ($.layoutCurrentDataScreen+1) + " / " + $.layoutNumDataScreens);
    	App.getApp().data_screen.updateLayout();
    	App.getApp().data_screen.requestUpdate();
    }

    function onBack() {
    	if (($.session != null) && $.session.isRecording()) {
    		// not used so far
			return true;
		}
		return false;
    }
}