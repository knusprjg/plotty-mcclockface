//
// All kind of functions used "everywhere".
//

using Toybox.Application;
using Toybox.Math;
using Toybox.System;
using Toybox.WatchUi;

// Debugging
(:debug)
function log(x) {
	var time = System.getClockTime();
	var time_string = "[" + time.hour.format("%02d") + ":" + time.min.format("%02d") + ":" + time.sec.format("%02d") + "] ";
	var str = "";
	if (x has :size) {
		for (var i=0; i<x.size(); i++) {
			str += x[i] + "\t";
		}
	} else {
		str = x.toString();
	}
	System.println(time_string + str);
}

(:debug)
function logMem(str) {
	var system_stats = System.getSystemStats();
	log("[MEM] " + str + "\tTotal memory: " + system_stats.totalMemory + "\tFree memory: " + system_stats.freeMemory + "\tUsed memory: " + system_stats.usedMemory);
}

// Switch for older devices
function setProperty(name, value) {
	if (Toybox.Application has :Storage) {
		// CIQ 2.4
		return Toybox.Application.Properties.setValue(name, value);
	} else {
		// Prior to CIQ 2.4
		return Toybox.Application.getApp().setProperty(name, value);
	}
}

// Switch for older devices
function getProperty(name) {
	if (Toybox.Application has :Storage) {
		// CIQ 2.4
		return Toybox.Application.Properties.getValue(name);
	} else {
		// Prior to CIQ 2.4
		return Toybox.Application.getApp().getProperty(name);
	}
}

// Get layout string
function getLayoutString(layout_no) {
    if (layout_no == LAY_GRAPH_TRACK) 				{return WatchUi.loadResource(Rez.Strings.graph_track);
    } else if (layout_no == LAY_GRAPH_ALTITUDE) 	{return WatchUi.loadResource(Rez.Strings.graph_altitude);
    } else if (layout_no == LAY_GRAPH_HEART_RATE) 	{return WatchUi.loadResource(Rez.Strings.graph_heart_rate);
    } else if (layout_no == LAY_GRAPH_SPEED) 		{return WatchUi.loadResource(Rez.Strings.graph_speed);
	} else if (layout_no == LAY_ALTITUDE) 			{return WatchUi.loadResource(Rez.Strings.altitude);
    } else if (layout_no == LAY_AMBIENT_PRESSURE) 	{return WatchUi.loadResource(Rez.Strings.ambient_pressure);
    } else if (layout_no == LAY_AVERAGE_CADENCE) 	{return WatchUi.loadResource(Rez.Strings.average_cadence);
    } else if (layout_no == LAY_AVERAGE_DISTANCE)	{return WatchUi.loadResource(Rez.Strings.average_distance);
    } else if (layout_no == LAY_AVERAGE_HEART_RATE) {return WatchUi.loadResource(Rez.Strings.average_heart_rate);
    } else if (layout_no == LAY_AVERAGE_POWER) 		{return WatchUi.loadResource(Rez.Strings.average_power);
    } else if (layout_no == LAY_AVERAGE_SPEED) 		{return WatchUi.loadResource(Rez.Strings.average_speed);
    } else if (layout_no == LAY_CALORIES) 			{return WatchUi.loadResource(Rez.Strings.calories);
    } else if (layout_no == LAY_CURRENT_CADENCE) 	{return WatchUi.loadResource(Rez.Strings.current_cadence);
    } else if (layout_no == LAY_CURRENT_HEART_RATE) {return WatchUi.loadResource(Rez.Strings.current_heart_rate);
    } else if (layout_no == LAY_CURRENT_POWER) 		{return WatchUi.loadResource(Rez.Strings.current_power);
    } else if (layout_no == LAY_CURRENT_SPEED) 		{return WatchUi.loadResource(Rez.Strings.current_speed);
    } else if (layout_no == LAY_ELAPSED_DISTANCE) 	{return WatchUi.loadResource(Rez.Strings.elapsed_distance);
    } else if (layout_no == LAY_ELAPSED_TIME) 		{return WatchUi.loadResource(Rez.Strings.elapsed_time);
    } else if (layout_no == LAY_ENERGY_EXPENDITURE) {return WatchUi.loadResource(Rez.Strings.energy_expenditure);
    } else if (layout_no == LAY_PACE) 				{return WatchUi.loadResource(Rez.Strings.pace);
    } else if (layout_no == LAY_TOTAL_ASCENT) 		{return WatchUi.loadResource(Rez.Strings.total_ascent);
    } else if (layout_no == LAY_TOTAL_DESCENT) 		{return WatchUi.loadResource(Rez.Strings.total_descent);
    } else if (layout_no == LAY_TRAINING_EFFECT) 	{return WatchUi.loadResource(Rez.Strings.training_effect);
    } else if (layout_no == LAY_TIME) 				{return WatchUi.loadResource(Rez.Strings.time);
    } else {
    	return null;
    }
}

function get_formated_activity_info(activity_info, attribute) {
	var no_value = "---";
	var value = null;

	if (attribute == $.LAY_ALTITUDE) {
		value = activity_info.altitude ? (activity_info.altitude).format("%4d") : no_value;
	} else if (attribute == $.LAY_AMBIENT_PRESSURE) {
		value = activity_info.ambientPressure ? (activity_info.ambientPressure / 100.0).format("%4d") : no_value;
	} else if (attribute == $.LAY_AVERAGE_CADENCE) {
		value = activity_info.averageCadence ? (activity_info.averageCadence) : no_value;
	} else if (attribute == $.LAY_AVERAGE_DISTANCE) {
		value = activity_info.averageDistance ? (activity_info.averageDistance / 1000.0).format("%0.2f") : no_value;
	} else if (attribute == $.LAY_AVERAGE_HEART_RATE) {
		value = activity_info.averageHeartRate ? (activity_info.averageHeartRate).format("%3d") : no_value;
	} else if (attribute == $.LAY_AVERAGE_POWER) {
		value = activity_info.averagePower ? (activity_info.averagePower).format("%4.1f") : no_value;
	} else if (attribute == $.LAY_AVERAGE_SPEED) {
		value = activity_info.averageSpeed ? (activity_info.averageSpeed * 3.6).format("%4.1f") : no_value;
	} else if (attribute == $.LAY_CALORIES) {
		value = activity_info.calories ? (activity_info.calories).format("%3d") : no_value;
	} else if (attribute == $.LAY_CURRENT_CADENCE) {
		value = activity_info.currentCadence;
	} else if (attribute == $.LAY_CURRENT_HEART_RATE) {
		value = activity_info.currentHeartRate ? (activity_info.currentHeartRate).format("%3d") : no_value;
	} else if (attribute == $.LAY_CURRENT_POWER) {
		value = activity_info.currentPower ? (activity_info.currentPower).format("%4.1f") : no_value;
	} else if (attribute == $.LAY_CURRENT_SPEED) {
		value = activity_info.currentSpeed ? (activity_info.currentSpeed * 3.6).format("%4.1f") : no_value;
	} else if (attribute == $.LAY_ELAPSED_DISTANCE) {
		value = activity_info.elapsedDistance ? (activity_info.elapsedDistance / 1000.0).format("%0.1f") : no_value;
	} else if (attribute == $.LAY_ELAPSED_TIME) {
		var sec = (activity_info.elapsedTime / 1000).toNumber() % 60;
		var min = (activity_info.elapsedTime / 60000).toNumber() % 60;
		var hour = (activity_info.elapsedTime / 3600000).toNumber() % 60;
		value = hour.format("%01d") + ":" + min.format("%02d") + ":" + sec.format("%02d");
	} else if (attribute == $.LAY_ENERGY_EXPENDITURE) {
		value = activity_info.energyExpenditure; // TODO
	} else if (attribute == $.LAY_PACE) {
		if ((activity_info.currentSpeed == null) || (activity_info.currentSpeed == 0)) {
			value = no_value;
		} else {
			var pace = 60.0 / (activity_info.currentSpeed * 3.6);
			var min = Math.floor(pace);
			var sec = (pace - min) * 60;
			value = min.format("%1d") + ":" + sec.format("%02d");
		}
	} else if (attribute == $.LAY_TOTAL_ASCENT) {
		value = activity_info.totalAscent ? (activity_info.totalAscent).format("%4d") : no_value;
	} else if (attribute == $.LAY_TOTAL_DESCENT) {
		value = activity_info.totalDescent ? (activity_info.totalDescent).format("%4d") : no_value;
	} else if (attribute == $.LAY_TRAINING_EFFECT) {
		value = activity_info.trainingEffect ? (activity_info.trainingEffect).format("%1.1f") : no_value;
	} else if (attribute == $.LAY_TIME) {
		var time = System.getClockTime();
		value = time.hour.format("%02d") + ":" +
				time.min.format("%02d") + ":" +
				time.sec.format("%02d");
	} else {
		return no_value;
	}

	return value;
}

function get_attribute_label(attribute) {
	if (attribute == $.LAY_ALTITUDE) {
		return "m";
	} else if (attribute == $.LAY_AMBIENT_PRESSURE) {
		return "hPa";
	} else if (attribute == $.LAY_AVERAGE_CADENCE) {
		return "rpm";
	} else if (attribute == $.LAY_AVERAGE_DISTANCE) {
		return "m";
	} else if (attribute == $.LAY_AVERAGE_HEART_RATE) {
		return "bpm avg";
	} else if (attribute == $.LAY_AVERAGE_POWER) {
		return "W avg";
	} else if (attribute == $.LAY_AVERAGE_SPEED) {
		return "km/h avg";
	} else if (attribute == $.LAY_CALORIES) {
		return "kcal";
	} else if (attribute == $.LAY_CURRENT_CADENCE) {
		return "rpm";
	} else if (attribute == $.LAY_CURRENT_HEART_RATE) {
		return "bpm";
	} else if (attribute == $.LAY_CURRENT_POWER) {
		return "W";
	} else if (attribute == $.LAY_CURRENT_SPEED) {
		return "km/h";
	} else if (attribute == $.LAY_ELAPSED_DISTANCE) {
		return "km";
	} else if (attribute == $.LAY_ELAPSED_TIME) {
		return "Timer";
	} else if (attribute == $.LAY_ENERGY_EXPENDITURE) {
		return "C/min";
	} else if (attribute == $.LAY_PACE) {
		return "min/km";
	} else if (attribute == $.LAY_TOTAL_ASCENT) {
		return "m";
	} else if (attribute == $.LAY_TOTAL_DESCENT) {
		return "m";
	} else if (attribute == $.LAY_TRAINING_EFFECT) {
		return "TE";
	} else {
		return "---";
	}
}



function write_graph_value(mem, i, value) {
	// store 4 values in one integer
	return (mem & !(0xFF << (8*i))) | (value << (8*i));
}

function read_graph_value(mem, i) {
	// number format: 0x11223344
	// 0xFF stands for null
	var val = (mem >> (8*i)) & 0xFF;
	return (val == 0xFF) ? null : val;
}
