using Toybox.System as Sys;
using Toybox.WatchUi as WatchUi;

class MenuConfigDataScreen extends MenuGeneric {
	var ds;

    function initialize(data_screen_no) {
    	self.ds = data_screen_no;

    	MenuGeneric.initialize();

    	self.setTitle(WatchUi.loadResource(Rez.Strings.settings));
    }

    function updateMenu() {
    	var graph_type = $.layoutDataScreenGraphConfigs[ds][GRAPH_TYPE];

        // Menu point for graph in selected data screen
        addMenuItem(
        		WatchUi.loadResource(Rez.Strings.graph),
        		self.getLayoutString(graph_type),
        		GRAPH_TYPE);
		addMenuItem(
				WatchUi.loadResource(Rez.Strings.graph_grid),
				$.layoutDataScreenGraphConfigs[ds][GRAPH_GRID] ?
						WatchUi.loadResource(Rez.Strings.graph_grid_on) :
						WatchUi.loadResource(Rez.Strings.graph_grid_off),
				GRAPH_GRID);
        if (graph_type != LAY_GRAPH_TRACK) {
			addMenuItem(
					WatchUi.loadResource(Rez.Strings.graph_x_axis),
					$.layoutDataScreenGraphConfigs[ds][GRAPH_X_AXIS] == TRACKER_DISTANCE ?
							WatchUi.loadResource(Rez.Strings.graph_x_distance) :
							WatchUi.loadResource(Rez.Strings.graph_x_time),
					GRAPH_X_AXIS);
			addMenuItem(
					WatchUi.loadResource(Rez.Strings.graph_avg_method),
					$.layoutDataScreenGraphConfigs[ds][GRAPH_AVG_METHOD] == TRACKER_AVERAGE ?
							WatchUi.loadResource(Rez.Strings.graph_avg_method_avg) :
							$.layoutDataScreenGraphConfigs[ds][GRAPH_AVG_METHOD] == TRACKER_MAX ?
									WatchUi.loadResource(Rez.Strings.graph_avg_method_max) :
									WatchUi.loadResource(Rez.Strings.graph_avg_method_min),
					GRAPH_AVG_METHOD);
			addMenuItem(
					WatchUi.loadResource(Rez.Strings.graph_avg_length),
					$.layoutDataScreenGraphConfigs[ds][GRAPH_AVG_LENGTH].toString() +
							(($.layoutDataScreenGraphConfigs[ds][GRAPH_X_AXIS] == TRACKER_DISTANCE) ? " m" : " s"),
					GRAPH_AVG_LENGTH);
		}
		if ($.layoutDataScreenGraphConfigs[ds][GRAPH_TYPE] == LAY_GRAPH_HEART_RATE) {
			addMenuItem(
					WatchUi.loadResource(Rez.Strings.graph_y_min),
					$.layoutDataScreenGraphConfigs[ds][GRAPH_Y_MIN].toString(),
					GRAPH_Y_MIN);
			addMenuItem(
					WatchUi.loadResource(Rez.Strings.graph_y_max),
					$.layoutDataScreenGraphConfigs[ds][GRAPH_Y_MAX].toString(),
					GRAPH_Y_MAX);
		}

        // Menu points for data fields in selected data screen
        for (var i=0; i<4; i+=1) {
	        addMenuItem(
	        		WatchUi.loadResource(Rez.Strings.field) + " " + (i+1),
        			self.getLayoutString($.layoutDataScreenFieldConfigs[ds][i]),
	        		100 + i);
		}
    }
}

class MenuConfigDataScreenDelegate extends MenuGenericDelegate {
	var data_screen_no;

	function initialize(data_screen_no) {
        MenuGenericDelegate.initialize();

        self.data_screen_no = data_screen_no;
	}

	function onMenuItem(item) {
		WatchUi.pushView(
				new MenuPicker(data_screen_no, item),
				new MenuPickerDelegate(data_screen_no, item),
				WatchUi.SLIDE_IMMEDIATE);

		return false; // return true to pop view when using Menu2
	}
}
