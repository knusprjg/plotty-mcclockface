//
// Functions used for decoding properties.
//

using Toybox.Application;
using Toybox.System;

function get_all_available_tracks() {
	/*
		Return all available tracks on the watch, i.e. route names and lengths in the format:
		[name1, length1, name2, length2, ...] or [] if no tracks are available.
	*/
	var routes = [];
	var coded_track;
	var name_end;
	var length_end;

	for (var t=0; t<3; t++) {
		coded_track = $.getProperty("coded_track_" + (t+1) + "_header");
		if ($.getProperty("coded_track_" + (t+1) + "_header").length() > 0 &&
			$.getProperty("coded_track_" + (t+1) + "_alt").length() > 0 &&
			$.getProperty("coded_track_" + (t+1) + "_points").length() > 0) {
			try {
				name_end = coded_track.find(",");
				routes.add(coded_track.substring(0, name_end));
				length_end = coded_track.substring(name_end+1, coded_track.length()).find(",");
				routes.add(coded_track.substring(name_end+1, name_end+length_end+1).toFloat() / 1000.0);
			} catch (e) {
			}
		}
	}
	return routes;
}

function decode_property_string() {
	/*
		Decode lon, lat, ele each at once. Not fast but goes easy on memory,
		which is the main constraint on older devices. Returns "null" if something goes wrong,
		otherwise an array with the queried results.

		Waypoint names code latitude, longitude and elevation information of the track in the following format:
		0 |     |   1 |
		01|23456|78901|234
		--+-----+-----+-----------------------------------------
		PP|AAAAA|OOOOO|EEE (Prefix|Latitude|Longitude|Elevation)
		The integers are coded as chars to the base given by the lengths of the CHARS array.

		Number: 0         1         2         3         4         5         6         7         8         9
		Number: 0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
		Code:   !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~

		For example: a => 10 and A => 36.
	*/

	// look for first graph of type "track"
	var ds = null;
	for (var i=0; i<$.layoutNumDataScreens; i++) {
		if ($.layoutDataScreenGraphConfigs[i][GRAPH_TYPE] == $.LAY_GRAPH_TRACK) {
			ds = i;
			break;
		}
	}
	// no need to decode anything if no graph is of type "track"
	if (ds==null || $.currentTrackName.length()==0) {
		return null;
	}

	var data_screen = Application.getApp().data_screen;

	/*
		Find currently set track in properties.
	*/
	var track_name;

	var coded_header;
	var found_track = false;
	var t; // track number (1-3)

	for (t=0; t<3; t++) {
		coded_header = $.getProperty("coded_track_" + (t+1) + "_header");
		try {
			found_track = coded_header.find($.currentTrackName) > -1;
			break;
		} catch (e) {
		}
	}

	if (found_track == false) {
		// track not found
		return null;
	} else {
		// reset track
		for (var p=0; p<data_screen.graphValues[ds].size(); p++) {
			data_screen.graphValues[ds][p] = 0xFFFFFFFF;
		}
		data_screen.track_lat = new [data_screen.layoutGraphWidth];
		data_screen.track_lon = new [data_screen.layoutGraphWidth];
	}

	/*
		Read the header of the current track.
	*/
	var track_length;
	var min_alt = null;
	var max_alt = null;
	var dlat = null;
	var dlon = null;
	var pt = 0;

	var sub = coded_header;
	for (var i=0; i<7; i++) {
		sub = sub.substring(pt, sub.length());
		pt = sub.find(",")+1;
		if (i==0)        {track_name = sub.substring(0, pt-1);
		} else if (i==1) {track_length = sub.substring(0, pt-1).toNumber();
		} else if (i==2) {min_alt = sub.substring(0, pt-1).toNumber();
		} else if (i==3) {max_alt = sub.substring(0, pt-1).toNumber();
		} else if (i==4) {data_screen.track_lat[0] = sub.substring(0, pt-1).toFloat();
		} else if (i==5) {data_screen.track_lon[0] = sub.substring(0, pt-1).toFloat();
		} else if (i==6) {dlat = sub.substring(0, pt-1).toFloat();
                          dlon = sub.substring(pt, sub.length()).toFloat();
		}
	}
	coded_header = null; // free mem

	/*
		Decode property strings.
	*/
	var mem_index = 0;
	var pos_in_mem = 0;

	var lat = data_screen.track_lat[0];
	var lon = data_screen.track_lon[0];
	var bdlat;
	var bdlon;
	var value;

	// decode altitude string
	var coded_alt = $.getProperty("coded_track_" + (t+1) + "_alt").toCharArray();
	// choose smaller value of either display width or the number of points available
	var num_points = data_screen.layoutGraphWidth>coded_alt.size() ?
			coded_alt.size() :
			data_screen.layoutGraphWidth;
	for (var p=0; p<num_points; p++) {
		value = ((coded_alt[p].toNumber()-33)/94.0*data_screen.graphBoxes[3] + 1).toNumber();

    	mem_index = p>>2; // (p/4).toNumber()
    	pos_in_mem = (p & 0x3) << 3; // (p % 4) * 8
    	data_screen.graphValues[ds][mem_index] = (data_screen.graphValues[ds][mem_index] & !(0xFF << pos_in_mem)) | (value << pos_in_mem);
	}
	coded_alt = null; // free mem

	// decode lat/lon string
	var coded_points = $.getProperty("coded_track_" + (t+1) + "_points").toCharArray();
	for (var p=0; p<num_points-1; p++) {
		value = coded_points[p].toNumber()-33;
		bdlat = (value&0x20) ? (value>>3) | 0xFFFFFFFC : (value>>3) & 0x3;
		bdlon = (value&0x04) ? value      | 0xFFFFFFFC : value & 0x3;
		lat += bdlat/3.0*dlat;
		lon += bdlon/3.0*dlon;
		data_screen.track_lat[p+1] = lat;
		data_screen.track_lon[p+1] = lon;
	}
	coded_points = null; // free mem

	data_screen.graphCurrentIndex[ds] = -1; // meaningless for track
   	// copy decoded (updated) values also to other graphs if they are of type "track"
	for (var k=ds; k<$.layoutNumDataScreens; k++) {
		if ($.layoutDataScreenGraphConfigs[k][GRAPH_TYPE] == $.LAY_GRAPH_TRACK) {
			data_screen.graphValues[k] = data_screen.graphValues[ds];
			data_screen.graphCurrentIndex[k] = -1;
			data_screen.absolute_min[k] = min_alt;
			data_screen.absolute_max[k] = max_alt;
		}
	}

	return true;
}

function reverseTrackDirection () {
	/*
		Reverses track direction.
	*/
	var data_screen = Application.getApp().data_screen;
	var w = data_screen.layoutGraphWidth;
	var temp_alt = [];
	var temp_lat = [];
	var temp_lon = [];
	var mem_index;
	var pos_in_mem;
	var value;

	for (var g=0; g<$.layoutNumDataScreens; g++) {
		if ($.layoutDataScreenGraphConfigs[g][GRAPH_TYPE] == $.LAY_GRAPH_TRACK) {
			temp_alt.addAll(data_screen.graphValues[g]);
			temp_lat.addAll(data_screen.track_lat);
			temp_lon.addAll(data_screen.track_lon);

			for (var p=0; p<w; p++) {
				// read altitude value
				mem_index = (w-1-p) >> 2;
				pos_in_mem = ((w-1-p) & 0x3) << 3;
				value = (temp_alt[mem_index] >> pos_in_mem) & 0xFF;

				// write altitude value
		    	mem_index = p>>2; // (p/4).toNumber()
		    	pos_in_mem = (p & 0x3) << 3; // (p % 4) * 8
		    	data_screen.graphValues[g][mem_index] = (data_screen.graphValues[g][mem_index] & !(0xFF << pos_in_mem)) | (value << pos_in_mem);

		    	// transpose lat/lon
				data_screen.track_lat[p] = temp_lat[w-1-p];
				data_screen.track_lon[p] = temp_lon[w-1-p];
			}
		}
	}
}
