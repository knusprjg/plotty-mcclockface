using Toybox.Application as App;
using Toybox.WatchUi as Ui;
using Toybox.System as Sys;

// Global variables
// Activity session
var session;
var sessionSport;
var sessionSubSport;

// Layout
var layoutCurrentDataScreen = 0;
var layoutNumDataScreens;
var layoutDataScreenFieldConfigs;
var layoutDataScreenGraphConfigs;
var layoutShowLimits;

// Track
var currentTrackName;
var trackDirectionForward;

// Layout constants
enum {
	LAY_GRAPH_TRACK = 0,
	LAY_GRAPH_ALTITUDE = 1,
	LAY_GRAPH_HEART_RATE = 2,
	LAY_GRAPH_SPEED = 3,
	LAY_ALTITUDE = 10,
	LAY_AMBIENT_PRESSURE = 11,
	LAY_AVERAGE_CADENCE = 12,
	LAY_AVERAGE_DISTANCE = 13,
	LAY_AVERAGE_HEART_RATE = 14,
	LAY_AVERAGE_POWER = 15,
	LAY_AVERAGE_SPEED = 16,
	LAY_CALORIES = 17,
	LAY_CURRENT_CADENCE = 18,
	LAY_CURRENT_HEART_RATE = 19,
	LAY_CURRENT_POWER = 20,
	LAY_CURRENT_SPEED = 21,
	LAY_ELAPSED_DISTANCE = 22,
	LAY_ELAPSED_TIME = 23,
	LAY_ENERGY_EXPENDITURE = 24,
	LAY_PACE = 25,
	LAY_TOTAL_ASCENT = 26,
	LAY_TOTAL_DESCENT = 27,
	LAY_TRAINING_EFFECT = 28,
	LAY_TIME = 29
}

enum {
	TRACKER_AVERAGE = 0,
	TRACKER_MAX = 1,
	TRACKER_MIN = 2
}

enum {
	TRACKER_DISTANCE = 0,
	TRACKER_TIME = 1
}

enum {
	GRAPH_TYPE = 0,
	GRAPH_AVG_METHOD = 1,
	GRAPH_AVG_LENGTH = 2,
	GRAPH_X_AXIS = 3,
	GRAPH_GRID = 4,
	GRAPH_Y_MIN = 5,
	GRAPH_Y_MAX = 6
}

/*
	TODO list

	- Coded chart as resource (data field possible..?)
	- Recalculated graph values when switching scale (check other glitches as well)
	- Check "has" attributes when adding buttons
    - Check if no menus exceed limits on FR235
    - Laps
    - Page indicator
*/

class PlottyApp extends App.AppBase {
	var data_screen;

    function initialize() {
        AppBase.initialize();
    }

    // onStart() is called on application start up
    function onStart(state) {
    	// restore layout variables from last session
    	loadLayoutSettings();
    	try {
    		$.currentTrackName = $.getProperty("current_track_name");
    		$.trackDirectionForward = $.getProperty("track_direction_forward");
    	} catch (ex) {
    		$.currentTrackName = "";
    		$.trackDirectionForward = true;
    	}
    	setCurrentTrack();
    }

    function onSettingsChanged() {
    	loadLayoutSettings();
    	setCurrentTrack();
    	data_screen.updateLayout();
    	data_screen.requestUpdate();
    }

    function setCurrentTrack() {
		/*
			Set current track name when app is started or settings changed.
			get_all_available_tracks() returns: [name1, length1, name2, ...],
			therefore possible cases:

			size of	     |                     |
			avail tracks | new track name      | comment
			-------------|---------------------|-------------------------------
			0            | ""                  | No tracks avail -> Reset to "".
			-------------|---------------------|-------------------------------
			2            | name of avail track | Auto set only track name available.
			-------------|---------------------|-------------------------------
			2 < x <=6    | current track       | If currentTrackName is still
			             | or ""               | in avail_tracks keep that,
			             |                     | otherwise reset.
		*/
		var avail_tracks = $.get_all_available_tracks();
		var new_track_name = "";
		if (avail_tracks.size() == 2) {
			new_track_name = avail_tracks[0];
		} else if ($.currentTrackName != "" && avail_tracks.size() > 2) {
			for (var i=0; i<avail_tracks.size(); i+=2) {
				if (avail_tracks[i] == $.currentTrackName) {
					new_track_name = $.currentTrackName;
				}
			}
		}
		$.currentTrackName = new_track_name;
		$.setProperty("current_track_name", new_track_name);
    }

    // onStop() is called when your application is exiting
    function onStop(state) {
    }

    // Return the initial view of your application here
    function getInitialView() {
    	data_screen = new DataScreen();
        return [data_screen, new DataScreenDelegate()];
    }

    function loadLayoutSettings() {
		$.layoutNumDataScreens = $.getProperty("num_data_screens");
		$.layoutShowLimits = $.getProperty("show_limits");

		// Initialize arrays (all data screens)
		$.layoutDataScreenFieldConfigs = new [5];
		$.layoutDataScreenGraphConfigs = new [5];

		for (var ds=0; ds<5; ds++) {
			// Initialize arrays (for each data screen)
			$.layoutDataScreenFieldConfigs[ds] = new [4];
			$.layoutDataScreenGraphConfigs[ds] = new [7];

			for (var df=0; df<4; df++) {
				$.layoutDataScreenFieldConfigs[ds][df] = $.getProperty("data_field_" + (ds+1) + "_" + (df+1));
			}

			$.layoutDataScreenGraphConfigs[ds][GRAPH_TYPE] = $.getProperty("graph_" + (ds+1) + "_type");
			$.layoutDataScreenGraphConfigs[ds][GRAPH_AVG_METHOD] = $.getProperty("graph_" + (ds+1) + "_avg_method");
			$.layoutDataScreenGraphConfigs[ds][GRAPH_AVG_LENGTH] = $.getProperty("graph_" + (ds+1) + "_avg_length");
			$.layoutDataScreenGraphConfigs[ds][GRAPH_X_AXIS] = $.getProperty("graph_" + (ds+1) + "_x_axis");
			$.layoutDataScreenGraphConfigs[ds][GRAPH_GRID] = $.getProperty("graph_" + (ds+1) + "_grid");
			$.layoutDataScreenGraphConfigs[ds][GRAPH_Y_MIN] = $.getProperty("graph_" + (ds+1) + "_y_min");
			$.layoutDataScreenGraphConfigs[ds][GRAPH_Y_MAX] = $.getProperty("graph_" + (ds+1) + "_y_max");
		}
    }
}