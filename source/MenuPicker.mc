using Toybox.Application as App;
using Toybox.System as Sys;
using Toybox.WatchUi as WatchUi;

class MenuPicker extends MenuGeneric {
	var ds;
	var trait;

    function initialize(data_screen_no, trait) {
		self.ds = data_screen_no;
    	self.trait = trait;

    	MenuGeneric.initialize();
    }

    function updateMenu() {
   		if (trait == $.GRAPH_TYPE) {
    		setTitle(WatchUi.loadResource(Rez.Strings.choose_graph));
	    	for (var graph_type=0; getLayoutString(graph_type)!=null; graph_type++) {
	    		// iterate over graph type
	    		addMenuItem(getLayoutString(graph_type), "", graph_type);
	    	}
    	} else if (trait == $.GRAPH_GRID) {
    		setTitle(WatchUi.loadResource(Rez.Strings.graph_grid));
	        addMenuItem(WatchUi.loadResource(Rez.Strings.graph_grid_on), "", true);
	        addMenuItem(WatchUi.loadResource(Rez.Strings.graph_grid_off), "", false);
    	} else if (trait == $.GRAPH_X_AXIS) {
    		setTitle(WatchUi.loadResource(Rez.Strings.graph_x_axis));
	        addMenuItem(WatchUi.loadResource(Rez.Strings.graph_x_time), "", $.TRACKER_TIME);
	        addMenuItem(WatchUi.loadResource(Rez.Strings.graph_x_distance), "", $.TRACKER_DISTANCE);
    	} else if (trait == $.GRAPH_AVG_METHOD) {
    		setTitle(WatchUi.loadResource(Rez.Strings.graph_avg_method));
	        addMenuItem(WatchUi.loadResource(Rez.Strings.graph_avg_method_avg), "", $.TRACKER_AVERAGE);
	        addMenuItem(WatchUi.loadResource(Rez.Strings.graph_avg_method_max), "", $.TRACKER_MAX);
	        addMenuItem(WatchUi.loadResource(Rez.Strings.graph_avg_method_min), "", $.TRACKER_MIN);
    	} else if (trait == $.GRAPH_AVG_LENGTH) {
    		setTitle(WatchUi.loadResource(Rez.Strings.graph_avg_length));
    		if ($.layoutDataScreenGraphConfigs[ds][GRAPH_X_AXIS] == TRACKER_TIME) {
    			createNumericMenu([1, 2, 5, 10, 60], " s");
    		} else {
    			createNumericMenu([1, 10, 50, 100, 200, 500, 1000], " m");
    		}
    	} else if (trait == $.GRAPH_Y_MIN) {
    		setTitle(WatchUi.loadResource(Rez.Strings.graph_y_min));
    		createNumericMenu([0, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160], " bpm");
    	} else if (trait == $.GRAPH_Y_MAX) {
    		setTitle(WatchUi.loadResource(Rez.Strings.graph_y_max));
    		createNumericMenu([0, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220], " bpm");
    	} else if (trait >= 100 && trait < 200) {
    		setTitle(WatchUi.loadResource(Rez.Strings.choose_data_field));
    		for (var df_type=10; getLayoutString(df_type) != null; df_type++) {
    			// iterate over data field types
    			addMenuItem(getLayoutString(df_type), "", df_type);
    		}
    	} else if (trait == 1000) {
	    	setTitle(Rez.Strings.num_data_screens);
	    	createNumericMenu([1, 2, 3, 4, 5], "");
    	} else if (trait == 1001) {
    		setTitle(Rez.Strings.track_direction);
    		addMenuItem(WatchUi.loadResource(Rez.Strings.forward), "", true);
    		addMenuItem(WatchUi.loadResource(Rez.Strings.backward), "", false);
    	}
    }

    function createNumericMenu(options, unit) {
    	for (var i=0; i<options.size(); i++) {
	        addMenuItem(
	        		options[i].toString() + unit.toString(),
	        		"",
	        		options[i]);
    	}
    }
}



class MenuPickerDelegate extends MenuGenericDelegate {
	var data_screen = App.getApp().data_screen;
	var ds;
	var trait;

	function initialize(data_screen_no, trait) {
        MenuGenericDelegate.initialize();

        self.ds = data_screen_no;
        self.trait = trait;
	}

	function onMenuItem(item) {
		if (trait < 100) {
			// Graph selected -> item == graph property
			$.layoutDataScreenGraphConfigs[ds][trait] = item;

			// store new settings on system memory
			$.setProperty("graph_" + (ds+1) + "_type", $.layoutDataScreenGraphConfigs[ds][GRAPH_TYPE]);
			$.setProperty("graph_" + (ds+1) + "_avg_method", $.layoutDataScreenGraphConfigs[ds][GRAPH_AVG_METHOD]);
			$.setProperty("graph_" + (ds+1) + "_avg_length", $.layoutDataScreenGraphConfigs[ds][GRAPH_AVG_LENGTH]);
			$.setProperty("graph_" + (ds+1) + "_x_axis", $.layoutDataScreenGraphConfigs[ds][GRAPH_X_AXIS]);
			$.setProperty("graph_" + (ds+1) + "_grid", $.layoutDataScreenGraphConfigs[ds][GRAPH_GRID]);
			$.setProperty("graph_" + (ds+1) + "_y_min", $.layoutDataScreenGraphConfigs[ds][GRAPH_Y_MIN]);
			$.setProperty("graph_" + (ds+1) + "_y_max", $.layoutDataScreenGraphConfigs[ds][GRAPH_Y_MAX]);

			// re-initialize graph to delete old values
			if (trait == $.GRAPH_TYPE) {
				data_screen.initGraph(ds);
				if (item == $.LAY_GRAPH_TRACK) {
					decode_property_string();
				}
			}
		} else if (trait >= 100 && trait < 200) {
			// Data field selected -> item == data field type
			var df = trait-100;
			$.layoutDataScreenFieldConfigs[ds][df] = item;

			// store new settings on system memory
    		$.setProperty("data_field_" + (ds+1) + "_" + (df+1), item);
		} else if (trait == 1000) {
			$.layoutNumDataScreens = item;

			// store new settings on system memory
			$.setProperty("num_data_screens", $.layoutNumDataScreens);
		} else if (trait == 1001) {
			if ($.trackDirectionForward != item) {
				$.reverseTrackDirection();
			}
			// store new settings on system memory
			$.trackDirectionForward = item;
			$.setProperty("track_direction_forward", $.trackDirectionForward);
		}

		return true; // return true to pop view when using Menu2
	}
}
