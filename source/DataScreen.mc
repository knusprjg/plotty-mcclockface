using Toybox.Activity as Activity;
using Toybox.Application as App;
using Toybox.Math as Math;
using Toybox.Position as Position;
using Toybox.System as Sys;
using Toybox.Timer as Timer;
using Toybox.UserProfile;
using Toybox.WatchUi as Ui;

class DataScreen extends Ui.View {
// Layout
	// device dependend settings
	const layoutDisplayHeight = System.getDeviceSettings().screenHeight;
	const layoutDisplayWidth = System.getDeviceSettings().screenWidth;

	const layoutGraphMargin =
			(System.getDeviceSettings().screenShape == System.SCREEN_SHAPE_RECTANGLE) ? 0 : 7;
	const layoutGraphDataFieldWidth = 66;

	var layoutGraphWidth = layoutDisplayWidth - 2*layoutGraphMargin;

	// derivated positions for layout objects
	var datafieldAligns = [
			Graphics.TEXT_JUSTIFY_RIGHT,
			Graphics.TEXT_JUSTIFY_LEFT,
			Graphics.TEXT_JUSTIFY_RIGHT,
			Graphics.TEXT_JUSTIFY_LEFT];
	var datafieldClips = [
			0,							0, 								layoutDisplayWidth/2,  layoutDisplayHeight/3 - 1,
			layoutDisplayWidth/2 + 1,	0, 								layoutDisplayWidth/2,  layoutDisplayHeight/3 - 1,
			0,							layoutDisplayHeight*2/3 + 2,	layoutDisplayWidth/2,  layoutDisplayHeight/3 - 2,
			layoutDisplayWidth/2 + 1,	layoutDisplayHeight*2/3 + 2,	layoutDisplayWidth/2,  layoutDisplayHeight/3 - 2];

	var datafieldLabelLocs = [
	  layoutDisplayWidth/2 - 10, layoutDisplayHeight/3-5.0-Graphics.getFontHeight(Graphics.FONT_MEDIUM)-Graphics.getFontHeight(Graphics.FONT_XTINY),
	  layoutDisplayWidth/2 + 10, layoutDisplayHeight/3-5.0-Graphics.getFontHeight(Graphics.FONT_MEDIUM)-Graphics.getFontHeight(Graphics.FONT_XTINY),
	  layoutDisplayWidth/2 - 10, layoutDisplayHeight/3*2+5.0+Graphics.getFontHeight(Graphics.FONT_MEDIUM),
	  layoutDisplayWidth/2 + 10, layoutDisplayHeight/3*2+5.0+Graphics.getFontHeight(Graphics.FONT_MEDIUM)];
	var datafieldValueLocs = [
	  layoutDisplayWidth/2 - 10, layoutDisplayHeight/3-5.0-Graphics.getFontHeight(Graphics.FONT_MEDIUM),
	  layoutDisplayWidth/2 + 10, layoutDisplayHeight/3-5.0-Graphics.getFontHeight(Graphics.FONT_MEDIUM),
	  layoutDisplayWidth/2 - 10, layoutDisplayHeight/3*2+5.0,
	  layoutDisplayWidth/2 + 10, layoutDisplayHeight/3*2+5.0];

	var graphBoxes = [
			layoutGraphMargin,
			layoutDisplayHeight/3,
			layoutDisplayWidth - 2*layoutGraphMargin,
			layoutDisplayHeight/3];

    var graph_points = layoutGraphWidth-layoutGraphDataFieldWidth;
    var graph_mem_width = Math.ceil(graph_points / 4.0);
    var graph_mem_width_track = Math.ceil(layoutGraphWidth / 4.0);

	var datafieldValues = new [4];
	var datafieldLabels = new [4];
	var graphCurrentIndex = [0, 0, 0, 0, 0];
	var graphUpdateRequest = true;
	var graphValues = new [5];

	var last_point = [0, 0, 0, 0, 0];

    var averaged_values = new [5];
    var average_counter = [0, 0, 0, 0, 0];
    var absolute_max = new [5];
    var absolute_min = new [5];

// Location handling
	// Constants
	var MAX_COST = 0.005; // TBD

	// Finding closest waypoint to current position
	var p_last = -1;

	var track_lat = new [layoutGraphWidth];
	var track_lon = new [layoutGraphWidth];

	var layout = [];
	var timer;
	var timer_fix_in_slow_devices;

	var init_step = 0;

	var drawCircle = 0;
	var cross_hair_color = Graphics.COLOR_TRANSPARENT;
	var uiInitWatchFace = true;
	var uiInitWatchFaceWorkaround = true;

	var hr_zones = UserProfile.getHeartRateZones(UserProfile.HR_ZONE_SPORT_GENERIC);
	var hr_colors = [
    			Graphics.COLOR_LT_GRAY,
    			Graphics.COLOR_BLUE,
    			Graphics.COLOR_GREEN,
    			Graphics.COLOR_ORANGE,
    			Graphics.COLOR_RED,
    			Graphics.COLOR_DK_RED];

    function initialize() {
        View.initialize();

    	for (var g=0; g<$.layoutNumDataScreens; g+=1) {
			initGraph(g);
		}

        updateLayout();

		timer = new Timer.Timer();
    	timer.start(method(:timerInitCallback), 100, true);
    }

  	function initGraph(g) {
  		graphValues[g] = ($.layoutDataScreenGraphConfigs[g][GRAPH_TYPE] == $.LAY_GRAPH_TRACK) ?
  				new [graph_mem_width_track] :
  				new [graph_mem_width];

		for (var l=0; l<graphValues[g].size(); l+=1) {
			graphValues[g][l] = 0xFFFFFFFF;
		}
  	}

    // Update layout (invoked when initializing and switiching views)
    function updateLayout() {
    	uiInitWatchFace = true;
    	graphUpdateRequest = true;
		layout = $.layoutDataScreenFieldConfigs[$.layoutCurrentDataScreen];

		// Initialize labels and values
		datafieldLabels = new [layout.size()];
		datafieldValues = new [layout.size()];

		// data field labels
		for (var i=0; i<4; i++) {
			datafieldLabels[i] = get_attribute_label(layout[i]);
		}

		for (var g=0; g<$.layoutNumDataScreens; g++) {
			if ($.layoutDataScreenGraphConfigs[g][GRAPH_TYPE] == $.LAY_GRAPH_HEART_RATE) {
				absolute_min[g] = $.layoutDataScreenGraphConfigs[g][$.GRAPH_Y_MIN];
				absolute_max[g] = $.layoutDataScreenGraphConfigs[g][$.GRAPH_Y_MAX];
			}
		}
    }

    // Location update
    function onPosition(currentLocation) {
    	//log("onPostion()");
		if ($.currentTrackName.equals("")) {
			return 0;
		}
    	var is_track_on_display = $.layoutDataScreenGraphConfigs[$.layoutCurrentDataScreen][$.GRAPH_TYPE] == $.LAY_GRAPH_TRACK;
    	var p;
    	// p_last defined in class
    	var cost;
    	var cost_last;
    	var current_position = currentLocation.position.toDegrees();
    	var current_lat = current_position[0];
    	var current_lon = current_position[1];
    	var track_n_points = track_lat.size();

		// reset p_last and request update (even if p_last "stays" 0 after reset)
		if (p_last<0) {
			p_last = 0;
			graphUpdateRequest |= is_track_on_display;
		}

    	// find closest waypoint to current location
    	p = (p_last+1) % track_n_points;
    	cost_last =
    		(track_lat[p_last]-current_lat).abs() +
    		(track_lon[p_last]-current_lon).abs();
    	try {
    		cost =
    			(track_lat[p]-current_lat).abs() +
    			(track_lon[p]-current_lon).abs();
    	} catch (e) {
    		// avoid null exception for track_lat/lon[p]
    		cost = 99999;
    	}

    	//log([current_lat, current_lon, p_last, track_lat[p_last], track_lon[p_last], cost_last, p, track_lat[p], track_lon[p], cost]);

    	// if cost > cost_last && cost_last < MAX_COST -> do nothing, stick with old crosshair
    	if (cost_last<cost && cost_last<MAX_COST) {
    		// last waypoint still optimal -> do nothing
    		cross_hair_color = Graphics.COLOR_RED;
    	} else if (cost<=cost_last && cost<MAX_COST) {
    		// next waypoint closer
    		cross_hair_color = Graphics.COLOR_RED;
    		p_last = p;
    		graphUpdateRequest |= is_track_on_display;
    	} else {
    		// none of the both other options is sufficient: search complete track for better match
    		for (var i=2; i<track_n_points; i++) {
    			p = (p_last+i) % track_n_points;
    			try {
	    			cost =
	    				(track_lat[p] - current_lat).abs() +
	    				(track_lon[p] - current_lon).abs();
	    		} catch (e) {
    				// avoid null exception for track_lat/lon[p]
	    			cost = 99999;
	    		}

    			if (cost<cost_last && cost<MAX_COST) {
    				// found a fitting waypoint
    				p_last = p;
    				cross_hair_color = Graphics.COLOR_RED;
    				break;
    			}
    		}
    		if (cross_hair_color==Graphics.COLOR_RED) {
    			// first time no matching point found or valid point found
    			graphUpdateRequest |= is_track_on_display;
    			cross_hair_color = (p==p_last) ? Graphics.COLOR_RED : Graphics.COLOR_BLUE;
    		}
    	}

    	if (graphUpdateRequest) {
    		requestUpdate();
    	}
    	return 1;

    	// log("/onPostion()");
	}

    // Called when this View is brought to the foreground. Restore
    // the state of this View and prepare it to be shown. This includes
    // loading resources into memory.
    function onShow() {
    	if (timer_fix_in_slow_devices == null) {
			timer_fix_in_slow_devices = new Timer.Timer();
	    	timer_fix_in_slow_devices.start(method(:timerFixLayoutInSlowDevices), 50, true);
	        updateLayout();
	        requestUpdate();
	    } else {
	        timer_fix_in_slow_devices.stop();
	        timer_fix_in_slow_devices = null;
	        updateLayout();
	        requestUpdate();
		}
    }

    function timerFixLayoutInSlowDevices() {
    	onShow();
    }

    function rescale_graph_values_y(g, y_min_old, y_max_old, y_min, y_max) {
    	if (y_max_old-y_min_old > 0) {
	    	var mem = graphValues[g];
	    	var y;
	    	// rescale graph values
	    	for (var mem_index=0; mem_index<mem.size(); mem_index++) {
	    		for (var pos_in_mem=0; pos_in_mem<32; pos_in_mem+=8) {
	    			y = (mem[mem_index] >> pos_in_mem) & 0xFF;
	    			if (y < 0xFF) {
	    				y = (((y_max_old-y_min_old)*y + y_min_old) - y_min) / (y_max-y_min);
	    				mem[mem_index] = (mem[mem_index] & !(0xFF << pos_in_mem)) | (Math.ceil(y).toNumber() << pos_in_mem).toNumber();
	    			}
	    		}
	    	}
	    	graphValues[g] = mem;
	    }
    }

    function adapt_scales(g) {
    	var min_step = $.layoutDataScreenGraphConfigs[g][GRAPH_TYPE] == $.LAY_GRAPH_SPEED ? 5.0/3.6 : 10.0;
    	var new_limit;
		// Absolute minima/maxima (needed for graph limits)
		if (($.layoutDataScreenGraphConfigs[g][GRAPH_TYPE] == $.LAY_GRAPH_HEART_RATE &&
				$.layoutDataScreenGraphConfigs[g][GRAPH_Y_MAX] > 0) ||
				averaged_values[g] == null) {
			// do nothing
		} else {
			if (absolute_min[g] == null) {
				if ($.layoutDataScreenGraphConfigs[g][GRAPH_TYPE] == $.LAY_GRAPH_ALTITUDE) {
					absolute_min[g] = (averaged_values[g]/min_step).toNumber()*min_step-30;
					absolute_max[g] = (averaged_values[g]/min_step).toNumber()*min_step+30;
				} else if ($.layoutDataScreenGraphConfigs[g][GRAPH_TYPE] == $.LAY_GRAPH_HEART_RATE) {
					absolute_min[g] = (averaged_values[g]/min_step).toNumber()*min_step-10;
					absolute_max[g] = (averaged_values[g]/min_step).toNumber()*min_step+50;
				} else if ($.layoutDataScreenGraphConfigs[g][GRAPH_TYPE] == $.LAY_GRAPH_SPEED) {
					absolute_min[g] = 0;
					absolute_max[g] = averaged_values[g] < 10.0/3.6 ?
							10.0/3.6 :
							Math.ceil(averaged_values[g]/min_step)*min_step;
				}
			} else if (averaged_values[g] < absolute_min[g]) {
				new_limit = Math.floor(averaged_values[g]/min_step)*min_step;
				rescale_graph_values_y(g, absolute_min[g], absolute_max[g], new_limit, absolute_max[g]);
				absolute_min[g] = new_limit;
			} else if (averaged_values[g] > absolute_max[g]) {
				new_limit = Math.ceil(averaged_values[g]/min_step)*min_step;
				rescale_graph_values_y(g, absolute_min[g], absolute_max[g], absolute_min[g], new_limit);
				absolute_max[g] = new_limit;
			}
			//log([g, absolute_min[g], absolute_max[g]]);
		}
	}

    function timerCallback () {
    	// log("onTimerCallback()");
    	//log("timerCallback()");
    	// feed DataTracker with measurement values
    	var activity_info = Activity.getActivityInfo();
    	var value = null;
    	var current_time = System.getTimer().toNumber() / 1000;

    	for (var g=0; g<$.layoutNumDataScreens; g++) {
    		if ($.layoutDataScreenGraphConfigs[g][GRAPH_TYPE ] != $.LAY_GRAPH_TRACK) {
    			// Query measurement value depending on graph type
    			value = [activity_info.altitude, activity_info.currentHeartRate, activity_info.currentSpeed][$.layoutDataScreenGraphConfigs[g][GRAPH_TYPE]-1];

    			// Apply averaging if necessary (min/max/avg)
    			if ($.layoutDataScreenGraphConfigs[g][GRAPH_AVG_LENGTH] < 2 ||
    					averaged_values[g] == null) {
    				// Store value if no averaging necessary (averaging length = 1),
    				// or if this is the first measurement after a reset.
    				averaged_values[g] = value;
    			} else if (value != null) {
    				// Actual averaging methods.
    				if ($.layoutDataScreenGraphConfigs[g][GRAPH_AVG_METHOD] == [$.TRACKER_AVERAGE]) {
    					averaged_values[g] = ((averaged_values[g]*average_counter[g]) + value) / (average_counter[g]+1);
    					average_counter[g] += 1;
    				} else if ($.layoutDataScreenGraphConfigs[g][GRAPH_AVG_METHOD] == [$.TRACKER_MAX] &&
    						averaged_values[g] < value) {
    					averaged_values[g] = value;
    				} else if ($.layoutDataScreenGraphConfigs[g][GRAPH_AVG_METHOD] == [$.TRACKER_MIN] &&
    						averaged_values[g] > value) {
    					averaged_values[g] = value;
    				}
    			}


    			// Check criterium for new plot line
    			var time_criteria = $.layoutDataScreenGraphConfigs[g][GRAPH_X_AXIS] == $.TRACKER_TIME &&
    					(current_time % $.layoutDataScreenGraphConfigs[g][GRAPH_AVG_LENGTH]) == 0;

    			var distance_criteria = $.layoutDataScreenGraphConfigs[g][GRAPH_X_AXIS] == $.TRACKER_DISTANCE &&
    					activity_info.elapsedDistance != null &&
    					activity_info.elapsedDistance.toNumber()/$.layoutDataScreenGraphConfigs[g][GRAPH_AVG_LENGTH] > last_point[g];

    			last_point[g] = distance_criteria == true ? last_point[g]+1 : last_point[g];

    			// Update graph values and reset averages
    			if (time_criteria || distance_criteria) {
    				adapt_scales(g);

    				graphCurrentIndex[g] = (graphCurrentIndex[g] + 1) % graph_points;

    				// Set new graph value
    				var y = averaged_values[g];

    				if (y != null && y > absolute_max[g]) {
    					// Exception for fixed scales. Values are clipped at absolute_max[g].
    					y = absolute_max[g];
    				} else if (y == null || y < absolute_min[g]) {
    					y = 0xFF; // null is encoded as 0xFF in "memory"
    				} else {
    					// scale for use in graph
    					y = ((y.toFloat()-absolute_min[g]) / (absolute_max[g]-absolute_min[g]) * graphBoxes[3] + 1).toNumber();
    				}
    				var mem_index = graphCurrentIndex[g] >> 2; // (i/4).toNumber()
    				var pos_in_mem = (graphCurrentIndex[g] & 0x3) << 3; // (i % 4) * 8
    				graphValues[g][mem_index] = (graphValues[g][mem_index] & !(0xFF << pos_in_mem)) | (y << pos_in_mem);

    				// reset averager
    				average_counter[g] = 0;
    				averaged_values[g] = null;

    				// Request graph update if when updated graph is currently shown
    				graphUpdateRequest |= (g == $.layoutCurrentDataScreen);
    			}
    		}
    	}
    	requestUpdate();
    	//log("/timerCallback()");
    	//log("/onTimerCallback()");
    }

    function timerInitCallback() {
    	//log("timerInitCallback()");
    	if (init_step == 0) {
    		init_step += 1;
			decode_property_string();
		    graphUpdateRequest = true;
		} else {
	    	// crude workaround to ensure that display is fully loaded
	    	uiInitWatchFaceWorkaround = false;

		    graphUpdateRequest = true;
	    	requestUpdate();

	    	timer.stop();
    		timer.start(method(:timerCallback), 1000, true);

	    	init_step = null;
	    }
    }

    // Update the view
    function onUpdate(dc) {
    	// log("onUpdate(dc)");
	    var value = null;

		// set variables
		var g = $.layoutCurrentDataScreen;
		var graph_type = $.layoutDataScreenGraphConfigs[g][GRAPH_TYPE];

		var w = graphBoxes[2];
		var h = graphBoxes[3];
		var llx = graphBoxes[0];
		var lly = graphBoxes[1]+h;
		var urx = graphBoxes[0]+w;
		var ury = graphBoxes[1];
		var graph_urx = (graph_type == $.LAY_GRAPH_TRACK) ? urx : llx+graph_points;

		var current_index = graphCurrentIndex[g];
		var mem_index;
		var pos_in_mem;

		var x = llx;

    	// background color
    	if (uiInitWatchFace ||
    			uiInitWatchFaceWorkaround) {
    		initWatchFace(dc);
    	}

    	// Retrieve data for datafields
    	var activity_info = Activity.getActivityInfo();

    	for (var i=0; i<4; i+=1) {
    		datafieldValues[i] = get_formated_activity_info(activity_info, layout[i]);
    	}

		// Update datafield(s) on screen
    	for (var i=0; i<4; i+=1) {
    		var iClip = i << 2;
    		var iLoc = i << 1;

    		// Clear field
    		dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_WHITE);
    		dc.fillRectangle(
    				datafieldClips[iClip],
    				datafieldClips[iClip+1],
    				datafieldClips[iClip+2],
    				datafieldClips[iClip+3]);

    		// Write value and label
    		dc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_TRANSPARENT);
    		dc.drawText(datafieldValueLocs[iLoc],
    					datafieldValueLocs[iLoc+1],
						Graphics.FONT_MEDIUM,
						datafieldValues[i],
						datafieldAligns[i]);
    		dc.setColor(Graphics.COLOR_DK_GRAY, Graphics.COLOR_TRANSPARENT);
    		dc.drawText(datafieldLabelLocs[iLoc],
    					datafieldLabelLocs[iLoc+1],
						Graphics.FONT_XTINY,
						datafieldLabels[i],
						datafieldAligns[i]);
    	}

    	// Update graph(s)
    	if (graphUpdateRequest) {
    		// log(["Found graphUpdateRequest...", g]);

    		// reset update request
    		graphUpdateRequest = false;

    		// clear graph and set clip to graph box
    		dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_WHITE);
    		dc.fillRectangle(llx, ury, w+1, h+1);

			// Draw graph
			dc.setColor(Graphics.COLOR_GREEN, Graphics.COLOR_GREEN);
			var data_points = (graph_type == $.LAY_GRAPH_TRACK) ? w : graph_points;
			for (var i=current_index+1; i<current_index+data_points+1; i++) {
				mem_index = (i % data_points) >> 2;
				pos_in_mem = (i % data_points) & 0x3;
				value = (graphValues[g][mem_index] >> (pos_in_mem << 3)) & 0xFF;

				if (value < 0xFF) {
					if (graph_type == $.LAY_GRAPH_HEART_RATE) {
						var hr = value.toFloat()/h*(absolute_max[g]-absolute_min[g])+absolute_min[g];
						for (var z=0; z<hr_zones.size(); z++) {
							if (hr < hr_zones[z]) {
								dc.setColor(hr_colors[z], hr_colors[z]);
								break;
							}
						}
					}
					dc.drawLine(x, lly, x, lly-value);
				}
				x++;
			}

			// draw grid
			if ($.layoutDataScreenGraphConfigs[g][$.GRAPH_GRID]) {
				drawGrid(
						dc,
						llx,
						lly,
						graph_urx,
						h,
						graph_type,
						$.layoutDataScreenGraphConfigs[g][$.GRAPH_Y_MIN],
						$.layoutDataScreenGraphConfigs[g][$.GRAPH_Y_MAX]);
			}

			// draw crosshair
			if (graph_type==$.LAY_GRAPH_TRACK && p_last>-1) {
				dc.setColor(cross_hair_color, Graphics.COLOR_TRANSPARENT);
				dc.drawLine(llx+p_last, lly, llx+p_last, ury-1);
				mem_index = (p_last % layoutGraphWidth) >> 2;
				pos_in_mem = (p_last % layoutGraphWidth) & 0x3;
				value = (graphValues[g][mem_index] >> (pos_in_mem << 3)) & 0xFF;
				dc.drawLine(llx, lly-value+1, urx+1, lly-value+1);
			}

    		// draw limits
	    	if ($.layoutShowLimits) {
	    		drawLimits(dc, g, llx, lly, ury);
	    	}
    	}

    	/*
    		Draw meter and label at every refresh. This avoids that the meter
    		freezes when the clock is not moving and the item is triggered by distance.
    	*/
		if (graph_type != $.LAY_GRAPH_TRACK) {
			var attribute = [null, $.LAY_ALTITUDE, $.LAY_CURRENT_HEART_RATE, $.LAY_CURRENT_SPEED][graph_type];

			// draw meter for latest value (value recycled from graph updating process)
			dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_WHITE);
			dc.fillRectangle(
					graph_urx,
					ury,
					layoutGraphDataFieldWidth,
					h);
			dc.setColor(Graphics.COLOR_DK_GREEN, Graphics.COLOR_TRANSPARENT);
			dc.drawLine(
					graph_urx,
					ury,
					graph_urx,
					lly+1);
			dc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_TRANSPARENT);
			dc.drawText(
					urx-2,
					ury + h/2.0 - Graphics.getFontHeight(Graphics.FONT_LARGE),
					Graphics.FONT_LARGE,
					get_formated_activity_info(activity_info, attribute),
					Graphics.TEXT_JUSTIFY_RIGHT);
			dc.drawText(
					urx - 2,
					ury + h/2.0 + 2,
					Graphics.FONT_XTINY,
					get_attribute_label(attribute),
					Graphics.TEXT_JUSTIFY_RIGHT);
		}

        // draw circle to inform user about GPS/tracking status
        if (drawCircle) {
        	if (Position.getInfo().accuracy < Position.QUALITY_POOR) {
        		dc.setColor(Graphics.COLOR_RED, Graphics.COLOR_TRANSPARENT);
        	} else {
        		dc.setColor(Graphics.COLOR_GREEN, Graphics.COLOR_TRANSPARENT);
        	}
        	dc.setPenWidth(5);

			if (System.getDeviceSettings().screenShape == System.SCREEN_SHAPE_RECTANGLE) {
				dc.drawRectangle(
						0,
						0,
						layoutDisplayWidth,
						layoutDisplayHeight);
			} else {
        		dc.drawCircle(
        				layoutDisplayWidth/2-1,
        				layoutDisplayHeight/2-1,
						layoutDisplayWidth/2-1);
			}
        	dc.setPenWidth(1);
        }
    }

    function drawLimits(dc, g, llx, lly, ury) {
    	//log("drawLimits()");
    	if (absolute_min[g] != null) {
			dc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_TRANSPARENT);
			dc.drawText(
					llx+2,
					lly-Graphics.getFontHeight(Graphics.FONT_XTINY)+2,
					Graphics.FONT_XTINY,
					(($.layoutDataScreenGraphConfigs[g][GRAPH_TYPE] == $.LAY_GRAPH_SPEED) ?
						(3.6*absolute_min[g]).toNumber() :
						absolute_min[g].toNumber()).toString(),
					Graphics.TEXT_JUSTIFY_LEFT
				);
			dc.drawText(
					llx+2,
					ury-2,
					Graphics.FONT_XTINY,
					(($.layoutDataScreenGraphConfigs[g][GRAPH_TYPE] == $.LAY_GRAPH_SPEED) ?
						(3.6*absolute_max[g]).toNumber() :
						absolute_max[g].toNumber()).toString(),
					Graphics.TEXT_JUSTIFY_LEFT
				);
		}
	}

    function drawGrid(dc, llx, lly, urx, h, type, y_min, y_max) {
    	//log("drawGrid()");
		dc.setColor(0xAFAFAF, Graphics.COLOR_TRANSPARENT);
		var grid_y;
    	if (type == $.LAY_GRAPH_HEART_RATE) {
    		var val = null;
			for (var z=0; z<hr_zones.size(); z++) {
				val = ((hr_zones[z]-y_min).toFloat()/(y_max-y_min) * h).toNumber();
				if (val > 0 && val < h) {
					dc.drawLine(llx, lly-val+1, urx, lly-val+1);
				}
			}
    	} else {
			var grid_v = 5;
			var grid_v_step = h / (grid_v-1);
			for (var i=1; i<grid_v-1; i++) {
				grid_y = lly - i*grid_v_step;
				dc.drawLine(llx, grid_y, urx, grid_y);
			}
    	}
    }

    function initWatchFace(dc) {
    	//log("initWatchFace()");
    	uiInitWatchFace = false;
    	graphUpdateRequest = true;

    	// clear complete screen
    	dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_WHITE);
    	dc.clear();

    	// basic grid
    	dc.setColor(Graphics.COLOR_DK_GREEN, Graphics.COLOR_TRANSPARENT);
    	dc.drawLine(layoutDisplayWidth/2, 0, layoutDisplayWidth/2, layoutDisplayHeight);
		dc.drawLine(0, layoutDisplayHeight/3 - 1, layoutDisplayWidth, layoutDisplayHeight/3 - 1); // upper boundary
		dc.drawLine(0, layoutDisplayHeight*2/3 + 1, layoutDisplayWidth, layoutDisplayHeight*2/3 + 1); // lower boundary
    }

	function enableGps() {
		//log("enableGps()");
	    Position.enableLocationEvents(Position.LOCATION_CONTINUOUS, method(:onPosition));
	}
}
