# Plotty McClockface

![Plotty McClockface](https://gitlab.com/knusprjg/plotty-mcclockface/wikis/uploads/e187e6071a284204974c898e1f843ced/track.png)

## About this repository:

Please report issues [here](https://gitlab.com/knusprjg/plotty-mcclockface/issues).

Before that, check out the [FAQ](https://gitlab.com/knusprjg/plotty-mcclockface/wikis/Plotty-McClockface).

Look here for [Plotty McWebinterface](https://gitlab.com/knusprjg/plotty-mcwebinterface).

## About the code:

Please don't look to closely at the code. It's certainly not my proudest code. ;) This partly due to the fact, that I'm only working in my sparetime on this project, but also down to deeper problems on the Garmin tools and devices.

It turned out that devices are quite tight on memory and computing resources and the compiler is far from optimal. For that reason I had to overhaul the code several times completely to be compatible with more devices. It also features several dirty tricks to save memory, like storing four 8 bit integers in one 32 bit integer variable.

The Menu structure is pretty heavy, I didn't find any other open source apps that included such a deep menu structure, so this might be worthwile to have a look at. General notes here are: Try to reuse strings, they don't go easy on the memory. Avoid too many classes and XML structures for the same reason. Also  Menu and Menu2 are not compatible, that's why I wrote wrapping classes ([MenuGeneric](https://gitlab.com/knusprjg/plotty-mcclockface/blob/master/source/MenuGeneric.mc), [MenuGeneric2](https://gitlab.com/knusprjg/plotty-mcclockface/blob/master/source/MenuGeneric2.mc), [MenuGenericDelegate](https://gitlab.com/knusprjg/plotty-mcclockface/blob/master/source/MenuGenericDelegate.mc), [MenuGenericDelegate2](https://gitlab.com/knusprjg/plotty-mcclockface/blob/master/source/MenuGenericDelegate2.mc)), that provide a common interface (eat this Garmin!). The definition of either MenuGeneric or MenuGeneric2 is chosen at compile time in the build scripts. Note however, that I don't try to mimic Menu2 with Menu. Instead newer stuff like sublabels is just ignored for the classic Menu.

## Garmin ConnectIQ store description:

*Please be fair and send me a message before giving a bad rating. I'm developing this app in my spare time and I have seen most of these watches only in the simulator. The preferred way for feedback is the GitLab repository, but I'm also happy to receive messages from the Garmin feedback button. Please just make sure to leave me a mail address, otherwise I have no option to respond.*

Plotty McClockface is your friend, no matter if it goes up or down. Plot heart rate, speed, altitude or the altitude profile of the track ahead of you. Choose graph scales to be either time or distance and freely set the intervals for updates.

Customize your screens with the built-in menu, or using Garmin Express (desktop) or Garmin Connect (mobile).

Since Garmin does not allow apps to directly access GPX tracks on the watch, it is necessary to upload your track altitude profile upfront using [Plotty McWebinterface](https://knusprjg.gitlab.io/plotty-mcwebinterface). Check out the readme on how it's done, it's super simple.

The best thing, it's open source! Feel free to have a look at the code, contribute or add translations.


The app requires the following permissions:
*  FIT files: Record tracks (all data will remain on your watch).
*  GPS location: Get the current location to show your position on the track.
*  Your Garmin Connect fitness profile: Required to visualize your heart rate zones in the graphs.
